const key = '5f96323678d05ff0c4eb264ef184556868e303b32a2db88ecbf15746e6f25e02';
const url = `https://api.unsplash.com/photos/?client_id=${key}&per_page=28`;

const fetchImages = async page => {
    const resp = await fetch(`${url}&per_page=9&page=${page}`);
    const data = await resp.json();
    if (resp.status >= 400) {
        throw new Error(data.errors);
    }
    return data;
};

export { fetchImages };
